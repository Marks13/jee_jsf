package monapp.bean;

import java.io.Serializable;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class AjaxBean implements Serializable {

    private static final long serialVersionUID = 5443351151396868724L;

    private String text = "";

    public AjaxBean() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        System.err.println("setText1 with " + text);
    }

    public String getNow() {
        return new Date().toString();
    }

}