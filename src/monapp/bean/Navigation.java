package monapp.bean;

import javax.faces.bean.ManagedBean;

@ManagedBean(name="navigation")
public class Navigation {

    public String courses() {
        return "courses?faces-redirect=true";
    }

}